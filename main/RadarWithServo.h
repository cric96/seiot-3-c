#ifndef __RADAR_WITH_SERVO__
#define __RADAR_WITH_SERVO__

#include "Radar.h"
#include "ProximitySensor.h"
#include <Servo.h>
class RadarWithServo : public Radar {
private:
  Servo * servo;
  ProximitySensor * prox;
  float angle;
  float velocity;
public:
  RadarWithServo(Servo * servo, ProximitySensor * prox, float omega) {
    this->servo = servo;
    this->prox = prox;
    this->velocity = omega;
  };
  void reset();
  void goOn(int dt);
  void sendDistance();
  void sendAngle();
  void setVelocity(float velocity);
  void start();
};

#endif

