#include "Timer.h"
#include "Arduino.h"
#include <MsTimer2.h>
volatile bool timerFlag;

Timer::Timer(){
  timerFlag = false;  
}
void changeTimer() {
  timerFlag=true;
}
void Timer::setupFreq(int freq){
  MsTimer2::set((1.0/freq*1000),changeTimer);
  MsTimer2::start();
}

/* period in ms */
void Timer::setupPeriod(int period){
  MsTimer2::set(period,changeTimer);
  MsTimer2::start();
}

void Timer::waitForNextTick(){
  /* wait for timer signal */
  while (!timerFlag){}
  timerFlag = false;
  
}

