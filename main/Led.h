#ifndef __LED__
#define __LED__

#define MAX_LIGHT 255
#define MIN_LIGHT 0 
//l'interfaccia del led
class Led {
public:
  //accende il led
  virtual void switchOn() = 0;
  //spegne il led
  virtual void  switchOff() = 0;  
};

#endif
