#include "Timer.h"
#include "Logger.h"
#include "Led.h"
#include "LedImpl.h"
#include "Radar.h"
#include "RadarWithServo.h"
#include "ProximitySensor.h"
#include "Sonar.h"
#define DT 100
#define OMEGA 1
#define MAX_MUL 2
#define MIN_MUL 0.5
#define INIT 1
#define DELTA_MUL 0.1
#define TO_GRAD_SEC(X,Y) ((X*Y)*180/3.14)/1000
#define LED 2
#define SERVO 9
#define PROX_TRIG 8

#define PROX_ECHO 7
Timer timer;
Radar * radar;
float multiplier = INIT;
Led * led = new LedImpl(LED);
enum {OFF, ACTIVE, WAITING} state;
enum {ON=49,STOP=50,RESTART=51,SHUTDOWN=52,PLUS=53,MINUS=54} message;
char command;
void setup(){
  Serial.begin(9600);
  timer.setupPeriod(DT);
  state = OFF;
  Logger::instance()->disable();
  Servo * servo = new Servo();
  servo->attach(SERVO);
  radar = new RadarWithServo(servo,new Sonar(PROX_ECHO,PROX_TRIG),TO_GRAD_SEC(OMEGA,multiplier));
  radar->reset();
}
void loop(){
  if(Serial.available()){
    command=Serial.read();
  }
  if(command == PLUS) {
    if(multiplier < MAX_MUL) multiplier += DELTA_MUL;
    radar->setVelocity(TO_GRAD_SEC(OMEGA,multiplier));
  } else if(command == MINUS) {
    if(multiplier > MIN_MUL) multiplier -= DELTA_MUL;
    radar->setVelocity(TO_GRAD_SEC(OMEGA,multiplier));
  }
  switch(state) {
    case OFF:
      if(command==ON){
        state=ACTIVE;
        led->switchOn();
        radar->start();
      }
      Logger::instance()->log("OFF");
    break; 
    case ACTIVE:
      radar->goOn(DT);
      Logger::instance()->log("A");
      radar->sendAngle();
      Logger::instance()->log("D");
      radar->sendDistance();
      if(command==SHUTDOWN){
        state=OFF;
        led->switchOff();
        radar->reset();
      } else if(command==STOP){
        state=WAITING;
      }
      Logger::instance()->log("ACTIVE");
    break;
    case WAITING:
    
      Logger::instance()->log("D");
      radar->sendDistance();
      if(command==SHUTDOWN){
        state=OFF;
        led->switchOff();
        radar->reset();
      } else if(command==RESTART){
        state=ACTIVE;
      }
      Logger::instance()->log("WAITING");
    break;
  }
  command = 0;
  timer.waitForNextTick();
}


