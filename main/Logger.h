#ifndef __LOGGER__
#define __LOGGER__

#include "Arduino.h"
class Logger {
private:
  bool _active;
  Logger(){
    this->_active = false;
  };
  static Logger * SINGLETON ;
public:
  static Logger * instance() {
     if(Logger::SINGLETON == 0) {
      Logger::SINGLETON = new Logger();
     }
     return Logger::SINGLETON;
  };
  void active() {
    this->_active = true;
  };
  void disable() {
    this->_active = false;
  };
  void log(String string) {
    if(this->_active) Serial.println(string);
  };
};
Logger * Logger::SINGLETON=0;
#endif

