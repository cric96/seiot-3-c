#include "RadarWithServo.h"
#include "Arduino.h"

#define MIDDLE_ANGLE 90
#define MIN_ANGLE 0
#define MAX_ANGLE 180
void RadarWithServo::reset() {
  this->angle = MIDDLE_ANGLE;
  servo->write(this->angle);
}

void RadarWithServo::start() {
  this->angle = MIN_ANGLE;
  servo->write(this->angle);
}

void RadarWithServo::sendDistance() {
  Serial.println(this->prox->getDistance());
}

void RadarWithServo::sendAngle() {
  Serial.println(this->angle);
}

void RadarWithServo::goOn(int dt) {
   this->angle += this->velocity * dt;
   
   if(this->angle >= MAX_ANGLE) {
      this->velocity = -this->velocity;
      this->angle = MAX_ANGLE;
   }
   if(this->angle <= MIN_ANGLE) {
      this->velocity = -this->velocity;
      this->angle = MIN_ANGLE;
   }
   servo->write(this->angle);
}
void RadarWithServo::setVelocity(float velocity){
  if(this->velocity < 0) {
    this->velocity = -velocity;
  } else {
    this->velocity = velocity;
  }
}

