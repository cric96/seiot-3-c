#ifndef __RADAR__
#define __RADAR__

class Radar {
public:
  virtual void reset() = 0;
  virtual void goOn(int dt) = 0;
  virtual void sendDistance() = 0;
  virtual void sendAngle() = 0;
  virtual void setVelocity(float velocity);
  virtual void start() = 0;
};
#endif 
